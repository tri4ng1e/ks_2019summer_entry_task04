#include <iostream>
#include <string>

#include <args.hxx>
#include <sqlite_modern_cpp.h>

using namespace std;
using namespace sqlite;


int main(int argc, char const *argv[]) {

	database db("db.sqlite");
    db << R"(CREATE TABLE IF NOT EXISTs "data" (
	    "id"	    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	    "client_id"	INTEGER NOT NULL,
	    "period"	INTEGER NOT NULL,
	    "value"	    INTEGER NOT NULL
	);)";
	db << R"(CREATE TABLE IF NOT EXISTs "clients" (
	    "id"	    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	    "name"    	TEXT NOT NULL
	);)";

	args::ArgumentParser parser("task04");
    parser.LongPrefix("/");
    parser.LongSeparator(" ");
    args::HelpFlag help(parser, "help", "Display this help menu", {"help"});
    args::ValueFlag<uint64_t> stat(parser, "stat", "get stats about client", {"stat"});
    args::Flag add(parser, "add", "add new data (id period value)", {"add"});
    args::Positional<uint64_t> id(parser, "id", "client ID");
    args::Positional<uint32_t> period(parser, "period", "period");
    args::Positional<float>    value(parser, "value", "value");

    try
    {
        parser.ParseCLI(argc, argv);
    }
    catch (args::Help)
    {
        std::cout << parser;
        return 0;
    }
    catch (args::ParseError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch (args::ValidationError e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    if (stat) {
    	auto ps_stat = db << R"(
    		SELECT
    		    MIN(val.delta) as delta,
    		    MAX(val.value) as max_value,
    		    (SELECT name FROM clients WHERE id = ?) as name
    		FROM
    		(SELECT
    		    value,
    		    ABS(value - LAG(value, 1) OVER(ORDER BY period)) as delta
    		FROM data
    		WHERE client_id = ?
    		ORDER BY period) val
    	)";

    	ps_stat << args::get(stat) << args::get(stat) >>
    	[](uint64_t delta, uint64_t max_value, string name) {
    		cout << "name: " << name << endl;
    		cout << "min delta: " << delta << endl;
    		cout << "max value: " << max_value << endl;
    	};

    }

    if (add) {
    	auto ps_add = db << R"(
   			INSERT INTO data (client_id, period, value) VALUES(?, ?, ?)
   	    )";

   	    ps_add << args::get(id) << args::get(period) << args::get(value);
    }


	return 0;
}